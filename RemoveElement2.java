public class RemoveElement2 {
    static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int j = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[j]) {
                
                nums[j] = nums[i];j++;
            }
        }

        return j + 1;
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 2, 3, 3, 3, 4, 4,4,4, 5,5};
        int newLength = removeDuplicates(nums);

        System.out.print("Output: ");
        for (int i = 0; i < newLength; i++) {
            System.out.print(nums[i] + " ");
        }
    }
}

