public class RemoveElement {

    public static int removeElement(int[] nums, int val) {
        int j = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[j] = nums[i];
                j++;
            }
        }

        return j; 
    }

    public static void main(String[] args) {
        int[] nums = {3, 2, 2, 4, 1, 2, 3};
        int valToRemove = 4;
        int newLength = removeElement(nums, valToRemove);

        System.out.print("Output: ");
        for (int i = 0; i < newLength; i++) {
            System.out.print(nums[i] + " ");
        }
    }
}
